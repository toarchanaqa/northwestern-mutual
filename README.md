## Dark Sky API Test
### Instructions
1. To install the dependencies. Run this command in terminal ``npm install``
2. Please specify appropriate values for the environment variables. These variables are present in .envShadow file. You can create .env file in root and set these variables & their values. The test will not run without these variables. It will give error message ``Error: Missing required configuration: GEO_API_KEY``. 
3. To run test, run this command in terminal ``npm test``
4. To check linting issues, run this command in terminal ``npm run lint``
5. To fix all auto-fixable linting issues, run this command in termincal ``npm run lint:fix``
6. ``npm test`` command would generate mochawesome report in the ``mochawesome-report`` folder. HTML report can be viewed by opening file ``mochawesome-report/mochawesome.html``