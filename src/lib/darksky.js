const request = require('supertest');
const config = require('../config/config');

module.exports = {
  forecast: ({ latitude, longitude, statusCode = 200 }) => request(config.DARK_SKY_API)
    .get(`/forecast/${config.DARK_SKY_API_KEY}/${latitude},${longitude}`)
    .expect(statusCode),
};
