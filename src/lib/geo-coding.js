const request = require('supertest');
const config = require('../config/config');

module.exports = {
  getGeoCodingForAddress: ({ address, output = 'json', statusCode = 200 }) => request(config.GEO_API)
    .get(`/${output}`)
    .query(`address=${address}&key=${config.GEO_API_KEY}`)
    .expect(statusCode),
};
