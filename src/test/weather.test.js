const { assert } = require('chai');
const darksky = require('../lib/darksky');
const geoCode = require('../lib/geo-coding');
const currentlySchema = require('../data/currently.json');

const schemaCheck = (expected, actual) => {
  const ok = Object.keys;
  const texpected = typeof expected;
  const tactual = typeof actual;
  return expected && actual && texpected === 'object' && texpected === tactual
    ? ok(expected).length === ok(actual).length
      && ok(expected).every((key) => schemaCheck(expected[key], actual[key]))
    : texpected === tactual;
};

describe('@regression, @geo-coding, @weather', () => {
  describe('tests for weather forecasting - by latitude & longitude', () => {
    let getForecastResponse;
    let location;
    const address = '425 Broadway New York, NY, 10013';

    before(async () => {
      const geoCodeResponse = await geoCode.getGeoCodingForAddress({ address });
      const geoCoding = geoCodeResponse.body;

      // as per google documentation, the API usually returns only 1 value in results array
      // But if address sent in the geocoding api is ambiguous, multiple results may be returned
      // For the purpose of this test, I am assuming test address is not ambiguous.
      const { geometry } = geoCoding.results[0];
      location = geometry.location;

      getForecastResponse = await darksky.forecast({
        latitude: location.lat,
        longitude: location.lng,
      });
    });

    it('verifies weather forecast returns 200', () => {
      assert.strictEqual(
        200,
        getForecastResponse.status,
        'status code returned is not 200',
      );
    });

    it('verifies latitude and longitude matches with address', () => {
      // assert latitude & longitude match between GeoCoding API & DarkSky API
      const { latitude, longitude } = getForecastResponse.body;
      assert.strictEqual(
        location.lat,
        latitude,
        'latitude values do not match',
      );
      assert.strictEqual(
        location.lng,
        longitude,
        'longitude values do not match',
      );
    });

    it('verifies the schema of currently object', () => {
      const {
        body: { currently },
      } = getForecastResponse;

      assert.hasAllDeepKeys(
        currentlySchema,
        currently,
        'response does not contain all the expected fields',
      );

      assert.isTrue(
        schemaCheck(currentlySchema, currently),
        'schema of response does not match expected schema',
      );
    });
  });
});
