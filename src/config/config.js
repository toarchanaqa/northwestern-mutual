require('dotenv').config();

const getRequiredString = (key) => {
  const value = process.env[key];
  if (!value) {
    throw new Error(`Missing required configuration: ${key}`);
  }
  return value;
};

module.exports = {
  GEO_API: getRequiredString('GEO_API'),
  GEO_API_KEY: getRequiredString('GEO_API_KEY'),
  DARK_SKY_API_KEY: getRequiredString('DARK_SKY_API_KEY'),
  DARK_SKY_API: getRequiredString('DARK_SKY_API'),
};
